package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class TwixPix {
	public static final int ROLL_UP = 1;
	public static final int WIPE_LEFT = 2;
	public static final int CROSSFADE = 3;
	public static final int DISSOLVE = 4;
	public static final int CLOCK = 5;
	public static final int BOX_OUT = 6;

	public static void main(String[] args) throws IOException {
		BufferedImage image1 = ImageIO.read(new File(
				"/Users/jbmoss84/Desktop/jrandi.jpg"));
		BufferedImage image2 = ImageIO.read(new File(
				"/Users/jbmoss84/Desktop/james.jpg"));

		// ImageIO.write(dissolve(image1, image2, 0
		// ), "jpg", new File(
		// "/Users/jbmoss84/Desktop/image3.jpg"));
		// ImageIO.write(crossfade(image1, image2, .60
		// ), "jpg", new File(
		// "/Users/jbmoss84/Desktop/image3.jpg"));
		// ImageIO.write(boxOut(image1, image2, .2), "jpg", new File(
		// "/Users/jbmoss84/Desktop/image3.jpg"));
		// ImageIO.write(wipeLeft(image1, image2, .8), "jpg", new File(
		// "/Users/jbmoss84/Desktop/image3.jpg"));
		// ImageIO.write(rollUp(image1, image2, .7), "jpg", new File(
		// "/Users/jbmoss84/Desktop/image3.jpg"));
		//ImageIO.write(clock(image1, image2, .7), "jpg", new File(
		//		"/Users/jbmoss84/Desktop/image3.jpg"));

	}

	public static BufferedImage dissolve(BufferedImage image1,
			BufferedImage image2, double proportion) {
		BufferedImage image3 = new BufferedImage(image1.getWidth(),
				image1.getHeight(), image1.getType());

		for (int r = 0; r < image1.getHeight(); r++) {
			for (int c = 0; c < image1.getWidth(); c++) {
				int packedColor1 = image1.getRGB(c, r);
				int packedColor2 = image2.getRGB(c, r);
				Color color1 = new Color(packedColor1);
				Color color2 = new Color(packedColor2);
				int red1 = color1.getRed();
				int green1 = color1.getGreen();
				int blue1 = color1.getBlue();
				int red2 = color2.getRed();
				int green2 = color2.getGreen();
				int blue2 = color2.getBlue();
				int red = (int) ((red1 * (1 - proportion)) + (red2 * proportion));
				int green = (int) ((green1 * (1 - proportion)) + (green2 * proportion));
				int blue = (int) ((blue1 * (1 - proportion)) + (blue2 * proportion));
				Color color = new Color(red, green, blue);
				int packedColor = color.getRGB();
				image3.setRGB(c, r, packedColor);

			}
		}
		return image3;

	}

	public static BufferedImage crossfade(BufferedImage image1,
			BufferedImage image2, double proportion) {
		BufferedImage image3 = new BufferedImage(image1.getWidth(),
				image1.getHeight(), image1.getType());

		for (int r = 0; r < image1.getHeight(); r++) {
			for (int c = 0; c < image1.getWidth(); c++) {
				if (proportion < .5) {
					int packedColor1 = image1.getRGB(c, r);
					Color color1 = new Color(packedColor1);
					int red1 = color1.getRed();
					int green1 = color1.getGreen();
					int blue1 = color1.getBlue();
					double distance = Math.abs(proportion - .5) * 2;
					int red = (int) (red1 * distance);
					int green = (int) (green1 * distance);
					int blue = (int) (blue1 * distance);
					Color color = new Color(red, green, blue);
					int packedColor = color.getRGB();
					image3.setRGB(c, r, packedColor);
				} else if (proportion >= .5) {
					int packedColor2 = image2.getRGB(c, r);
					Color color2 = new Color(packedColor2);
					int red2 = color2.getRed();
					int green2 = color2.getGreen();
					int blue2 = color2.getBlue();
					double distance = Math.abs(proportion - .5) * 2;
					int red = (int) (red2 * distance);
					int green = (int) (green2 * distance);
					int blue = (int) (blue2 * distance);
					Color color = new Color(red, green, blue);
					int packedColor = color.getRGB();
					image3.setRGB(c, r, packedColor);
				}
			}
		}
		return image3;
	}

	public static BufferedImage boxOut(BufferedImage image1,
			BufferedImage image2, double proportion) {
		BufferedImage image3 = new BufferedImage(image1.getWidth(),
				image1.getHeight(), image1.getType());

		for (int r = 0; r < image1.getHeight(); r++) {
			for (int c = 0; c < image1.getWidth(); c++) {
				int packedColor1 = image1.getRGB(c, r);
				image3.setRGB(c, r, packedColor1);
			}
		}
		for (int r = (int) ((image1.getHeight() * (1 - proportion)) / 2); r < ((image1
				.getHeight() / 2) + (image1.getHeight() / 2) * proportion); r++) {
			for (int c = (int) ((image1.getWidth() * (1 - proportion)) / 2); c < ((image1
					.getWidth() / 2) + (image1.getWidth() / 2) * proportion); c++) {
				int packedcolor = image2.getRGB(c, r);
				image3.setRGB(c, r, packedcolor);

			}
		}
		return image3;
	}

	public static BufferedImage wipeLeft(BufferedImage image1,
			BufferedImage image2, double proportion) {
		BufferedImage image3 = new BufferedImage(image1.getWidth(),
				image1.getHeight(), image1.getType());

		for (int r = 0; r < image1.getHeight(); r++) {
			for (int c = 0; c < image1.getWidth() * (1 - proportion); c++) {
				int packedColor1 = image1.getRGB(c, r);
				image3.setRGB(c, r, packedColor1);
			}
		}
		for (int r = 0; r < (image1.getHeight()); r++) {
			for (int c = (int) (image1.getWidth() * (1 - proportion)); c < image1
					.getWidth(); c++) {
				int packedcolor = image2.getRGB(c, r);
				image3.setRGB(c, r, packedcolor);

			}
		}

		return image3;
	}

	public static BufferedImage rollUp(BufferedImage image1,
			BufferedImage image2, double proportion) {
		BufferedImage image3 = new BufferedImage(image1.getWidth(),
				image1.getHeight(), image1.getType());
		int r2 = (int) ((image1.getHeight() * (proportion)) -1);
		int proportionLine = (int) (image1.getHeight() * (1 - proportion)); 
		for (int r = 0; r < proportionLine; r++) {
			if (r2 < image1.getHeight()) {
				r2++;
				
			}
			for (int c = 0; c < image1.getWidth(); c++) {
				int packedColor1 = image1.getRGB(c, r2);
				image3.setRGB(c, r, packedColor1);
				 r2 = (int)(image1.getHeight()-proportionLine+r);
				//System.out.println(r2);
			}
		}
		int r3= -1;
		for (int r = proportionLine; r < image1
				.getHeight(); r++) {
			r3++;
			for (int c = 0; c < image1.getWidth(); c++) {
				int packedcolor = image2.getRGB(c, r3);
				image3.setRGB(c, r, packedcolor);

			}
		}

		return image3;
	}

	public static BufferedImage clock(BufferedImage image1,
			BufferedImage image2, double proportion) {
		BufferedImage image3 = new BufferedImage(image1.getWidth(),
				image1.getHeight(), image1.getType());

		for (int r = 0; r < image1.getHeight(); r++) {
			for (int c = 0; c < image1.getWidth(); c++) {
				int distancex = (r - image1.getHeight() / 2);
				int distancey = (c - image1.getWidth() / 2);
				double theta = Math.atan2(distancex, distancey); 
				double radians =	(Math.PI / 2);
				double centralAngle = theta + radians;
				if (centralAngle < 0) {
					centralAngle = centralAngle + (2 * Math.PI);
					
				}
				double normalizedAngle = centralAngle / (Math.PI*2);
				
				if(normalizedAngle >= proportion){
					int packedColor1 = image1.getRGB(c, r);
					image3.setRGB(c, r, packedColor1);
				} else {
					int packedcolor = image2.getRGB(c, r);
					image3.setRGB(c, r, packedcolor);
				}
				

			}

		}
		return image3;
	}

	public static BufferedImage transition(BufferedImage image1,
			BufferedImage image2, double proportion, int t) {
		if (t == ROLL_UP) {
			return rollUp(image1, image2, proportion);
		} else if (t == WIPE_LEFT) {
			return wipeLeft(image1, image2, proportion);
		} else if (t == CROSSFADE) {
			return crossfade(image1, image2, proportion);
		} else if (t == DISSOLVE) {
			return dissolve(image1, image2, proportion);
		} else if (t == CLOCK) {
			return clock(image1, image2, proportion);
		} else if (t == BOX_OUT) {
			return boxOut(image1, image2, proportion);
		}
		return null;
	}

}
