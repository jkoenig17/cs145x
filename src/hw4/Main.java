package hw4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Main {
	public static void main(String[] args) throws IOException{
		BufferedImage image1 = ImageIO.read(new File(
				"/Users/jbmoss84/Desktop/jrandi.jpg"));
		BufferedImage image2 = ImageIO.read(new File(
				"/Users/jbmoss84/Desktop/james.jpg"));
		new SplittinImage(image1, image2, TwixPix.CROSSFADE);
	}

}
