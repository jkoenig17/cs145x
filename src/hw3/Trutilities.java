package hw3;

import java.util.Scanner;

public class Trutilities {
	public static void main(String[] args) {
		 System.out.println(inBox(1, 6, 7, 9, 100, 150));
		 System.out.println(inCircle(1, 2, 3, 4, 5));
		 System.out.println(isGrayscale("#4a4a4a"));
		System.out.println(isNorth(45, "70°40'20"));
		System.out.println(isBefore(2000, 10, 4, 2000, 10, 4));
		System.out.println(isEqualEnough(5, 6, .9));
		System.out.println(isPowerOfTen(1));
		System.out.println(isGameOver("quit"));
	}

	public static boolean inBox(double x1, double y1, double x2, double y2,
			double x3, double y3) {
		boolean inside = (x3 >= x1 && x3 <= x2) && (y3 >= y1 && y3 <= y2);
		return inside;
	}

	public static boolean inCircle(double x1, double y1, double r, double x2,
			double y2) {
		boolean inside = Math.abs(x1 - x2) <= r && Math.abs(y1 - y2) <= r;

		return inside;
		
	}

	public static boolean isGrayscale(String color) {
		Scanner in = new Scanner(color);
		in.useDelimiter("#");
		color = in.next();
		String r = color.substring(0, 2);
		String g = color.substring(2, 4);
		String b = color.substring(4, 6);
		boolean c = r.equals(g) && r.equals(b);
		return c;

	}

	public static boolean isNorth(double latitude, String dms) {
		Scanner in= new Scanner(dms);
		in.useDelimiter("°|'|\"");
		double degree1= Double.parseDouble(in.next());
		double minute1= Double.parseDouble(in.next());
		double second1= Double.parseDouble(in.next());
		boolean dd = degree1 + (minute1 / 60)+ (second1 / 3600) < latitude;
		return dd;
	}

	public static boolean isBefore(int year1, int month1, int day1, int year2, int month2, int day2) {
			if(year1<year2){
				return true;
			} else if(year1==year2 && month1<month2){
				return true;
			} else if (year1==year2&&month1==month2 && day1<day2){
				return true;
			} else if (year1==year2&&month1==month2 && day1==day2){
				return false;
			} else
				return false;
			
				
	}

	public static boolean isEqualEnough(double number1, double number2,
			double threshold) {
		boolean f = Math.abs((number2 - number1)) <= threshold
				|| Math.abs((number1 - number2)) <= threshold;
		return f;
	}

	public static boolean isPowerOfTen(int number) {
		boolean power = Math.log10(number)==0 || Math.log10(number)==1 || Math.log10(number)==2 || Math.log10(number)==3 || Math.log10(number)==4 || Math.log10(number)==5 || Math.log10(number)==6 || Math.log10(number)==7 || Math.log10(number)==8 || Math.log10(number)==9;
		return power;
		
	}

	public static boolean isGameOver(String command) {
		command = command.toLowerCase();
		String q = "quit";
		String e = "exit";
		String d = "done";
		boolean done = command.equals(q) || command.equals(e)
				|| command.equals(d);
		return done;
	}
}
