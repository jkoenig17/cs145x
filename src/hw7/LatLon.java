package hw7;

public class LatLon {
	private double latitude;
	private double longitude;

	public LatLon(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;	
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double distanceTo(LatLon two) {
	 double y =  two.getLatitude() - this.latitude;
	 double x =  two.getLongitude() - this.longitude;
	 double y1 =  Math.pow(y, 2);
	 double x1 =  Math.pow(x, 2);
	 double z = y1+ x1;
	 double distance = Math.sqrt(z);
		return distance;
	}
	
}
