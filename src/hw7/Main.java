package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
public static void main(String[] args) {
	
}
	public static void clusterAndMap(ArrayList<Place> places, int k, File red) throws FileNotFoundException {
		KMeans.cluster(places, k);
		KmlUtilities.write(red, places);
	}
	
	public static void clusterAndMap(File blue, int k, File red) throws FileNotFoundException {
		Scanner in = new Scanner(blue);
		ArrayList<Place> yes = new ArrayList<Place>();
		while(in.hasNextLine()){
			String[] run = in.nextLine().split("\t");
			LatLon laddy = new LatLon(Double.parseDouble(run[2]), Double.parseDouble(run[3]));
			Place funny = new Place(run[0], run[1], laddy);
			yes.add(funny);
		}
		in.close();
		
		KMeans.cluster(yes, k);
		KmlUtilities.write(red, yes);
		
	}

}
