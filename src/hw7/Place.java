package hw7;

public class Place {
	private String name;
	private String description;
	private LatLon location;
	private int clusterID;
	
	public 	Place(String name, String description, LatLon location) {
		this.name = name;
		this.description = description;
		this.location = location;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public LatLon getLocation() {
		return location;
	}
	
	public void setClusterID(int clusterID) {
		this.clusterID = clusterID;
	}
	
	public int getClusterID() {
		return clusterID;
	}
	
	public String toString() {
		return String.format("%s %s %s %s", name, location.getLatitude(), location.getLongitude(), clusterID);
		
	}
}
