package hw7;

import java.util.ArrayList;

public class KMeans {
	public static int indexOfNearestMean(LatLon[] means, LatLon location) {
		int x = 0;
		LatLon yes = means[0];
		double distance = yes.distanceTo(location);
		for (int i = 0; i < means.length; i++) {
			double distance1 = 0;
			LatLon yes1 = means[i];
			distance1 = yes1.distanceTo(location);
			if (distance1 < distance) {
				x = i;
				yes = means[i];
				distance = yes.distanceTo(location);
			}
		}

		return x;
	}

	public static boolean updateMembershipAndMeans(LatLon[] means,
			ArrayList<Place> places) {
		Cluster[] red = new Cluster[means.length];
		boolean difference = false;
		for (int i = 0; i < means.length; i++) {
			red[i] = new Cluster();
		}

		for (int d = 0; d < places.size(); d++) {
			int j = indexOfNearestMean(means, places.get(d).getLocation());
			
			if (places.get(d).getClusterID() != j) {
				difference = true;
			}
			places.get(d).setClusterID(j);
			red[j].add(places.get(d).getLocation());
		}

		for (int a = 0; a < red.length; a++) {
			means[a] = red[a].getMeanLocation();
		}

		return difference;

	}

	public static void cluster(ArrayList<Place> places, int k) {
		LatLon[] numbers = new LatLon[k];
		for (int i = 0; i < k; i++) {
			numbers[i] = places.get(i).getLocation();
		}
		while (updateMembershipAndMeans(numbers, places)) {

		}
	}

}
