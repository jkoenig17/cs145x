package hw7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class KmlUtilities {
		public static void write(File red, ArrayList<Place> places) throws FileNotFoundException {
			PrintWriter out = new PrintWriter(red);
			out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.println("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
			out.println("<Document>");
			out.println("<name>KMap</name>");
			out.println("<Style id=\"cluster0\"><IconStyle>");
			out.println("  <color>ff00ff00</color>");
			out.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
			out.println("</IconStyle></Style>");
			out.println("<Style id=\"cluster1\"><IconStyle>");
			out.println("  <color>ff999999</color>");
			out.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
			out.println("</IconStyle></Style>");
			out.println("<Style id=\"cluster2\"><IconStyle>");
			out.println("  <color>ff0000ff</color>");
			out.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
			out.println("</IconStyle></Style>");
			out.println("<Style id=\"cluster3\"><IconStyle>");
			out.println("  <color>ff0099ff</color>");
			out.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
			out.println("</IconStyle></Style>");
			out.println("<Style id=\"cluster4\"><IconStyle>");
			out.println("  <color>ff00ffff</color>");
			out.println("  <Icon><href>http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png</href></Icon>");
			out.println("</IconStyle></Style>");
			for(int i = 0; i < places.size(); i++) {
			out.println("<Placemark>");
			out.println("  <name>" + places.get(i).getName() + "</name>");
			out.println("  <description>" + places.get(i).getDescription() + "</description>");
			out.printf("  <styleUrl>#cluster%d</styleUrl>", places.get(i).getClusterID());
			out.println("");
			out.printf("  <Point><coordinates>%f,%f</coordinates></Point>", places.get(i).getLocation().getLongitude() , places.get(i).getLocation().getLatitude());
			out.println("");
			out.println("</Placemark>");
			}
			out.println("</Document>");
			out.println("</kml>");
			
			
			out.close();
			
	}
}
