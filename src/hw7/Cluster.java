package hw7;

import java.util.ArrayList;

public class Cluster {

	private ArrayList<LatLon> location = new ArrayList<>();

	public Cluster() {
		{
			
		}
	}

	public void add(LatLon two) {
		location.add(two);
	}

	public LatLon getMeanLocation() {
		double longitude = 0;
		double latitude = 0;
		for(int i = 0; i < location.size(); i++){
			LatLon wrap = location.get(i);
			longitude += wrap.getLongitude();
			latitude += wrap.getLatitude();
		}
		
		double longitude1 = longitude / (location.size());
		double latitude1 = latitude / (location.size());
		LatLon mean = new LatLon(latitude1, longitude1);
	return mean;	
	}
	


}
