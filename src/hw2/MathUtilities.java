package hw2;

public class MathUtilities {
	public static void main(String[] args) {
		
	}
	public static double getHypotenuseDifference(double x1, double y1) {
		double difference= Math.sqrt(Math.pow(x1,2)+Math.pow(y1, 2));
		difference= x1+y1-difference;
		return difference;
	}
	public static double lerp(double x1, double y1, double x2, double y2, double x3) {
		double slope= (y2-y1)/(x2-x1);
		double yint= y1-(slope*x1);
		double y3= slope*x3+yint;
		return y3;
	}
}

