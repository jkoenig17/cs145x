package hw2;

public class ShapeUtilities {
	public static void main(String[] args) {
		
		
	
	}
	public static double getCircleArea(double radius) {
		double areaCircle = Math.PI*(Math.pow(radius,2));
		return areaCircle;
	}
	
	public static double getAnnulusArea(double outradius, double inradius) {
		double annulusarea= (Math.PI*(Math.pow(outradius,2))-Math.PI*(Math.pow(inradius,2)));
		return annulusarea;
	}
}
