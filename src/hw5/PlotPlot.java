package hw5;

import java.util.ArrayList;


public class PlotPlot {
	public static void main(String[] args) {
		// System.out.println(getPathOfFirsts(getChoicesForAllPages("2|10|||4|5|6|7|8|9|")));
		 
		//dotEndPages(getChoicesForAllPages("2|3|4|5|"));
		//dot(getChoicesForAllPages("2,3|4|5|6|7|8|9|10|||"));
	}

	public static int[] getChoicesForOnePage(String page) {

		if (page.equals("p")) {
			return null;
		} else if (page.equals("")) {
			int[] empty = new int[0];
			return empty;
		} else {
			String[] split = page.split(",");
			int[] numbers = new int[split.length];
			for (int i = 0; i < split.length; i++) {
				numbers[i] = Integer.parseInt(split[i]) - 1;

			}

			return numbers;
		}
	}

	public static int[][] getChoicesForAllPages(String allpages) {
		String[] split = allpages.split("\\|", -1);
		int[][] numbers = new int[split.length][];
		for (int b = 0; b < numbers.length; b++) {

			numbers[b] = getChoicesForOnePage(split[b]);

		}
		return numbers;

	}

	public static ArrayList<Integer> getForkPages(int[][] numbers) {
		ArrayList<Integer> forkpages = new ArrayList<Integer>();
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == null) {
				int[] empty = new int[0];
				numbers[i] = empty;
			}
			int length = numbers[i].length;

			if (length > 1) {
				forkpages.add(i);
			}
		}

		return forkpages;
	}

	public static ArrayList<Integer> getEndPages(int[][] numbers) {
		ArrayList<Integer> endpages = new ArrayList<Integer>();
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] != null) {
				if (numbers[i].length == 0) {
					endpages.add(i);
				}
			}
		}

		return endpages;
	}

	public static ArrayList<Integer> getOrphanPages(int[][] numbers) {
	
		ArrayList<Integer> orphan = new ArrayList<Integer>();
		boolean[] truth = new boolean[numbers.length];
			
		int[] empty = new int[0];
		
		for(int i = 0; i < numbers.length; i++){
			if(numbers[i] == null){		
				numbers[i] = empty;
				truth[i] = true;
				
				
			}
			for ( int c = 0 ; c < numbers[i].length; c++) { 
				int[] yep = numbers[i];
				int yes = yep[c];
				truth[yes] = true;
			}
		}
		
		for(int d= 1; d < numbers.length; d++){
			if (truth[d] == false){
				orphan.add(d);
			}
			
			}
		return orphan;
	}

	public static void dotEndPages(int[][] numbers) {
		ArrayList<Integer> hello = new ArrayList<Integer>(getEndPages(numbers));
		for (int i = 0; i < hello.size(); i++) {
			System.out.println("  " + (hello.get(i) + 1)  + " [peripheries=2];");
		}

	}

	public static void dotOrphanPages(int[][] numbers) {
		ArrayList<Integer> hello = new ArrayList<Integer>(
				getOrphanPages(numbers));
		for (int i = 0; i < hello.size(); i++) {
			System.out.println("  " + (hello.get(i) + 1) + " [style=dashed];");
		}

	}

	public static void dotPlot(int[][] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == null) {
				int[] empty = new int[0];
				numbers[i] = empty;
			}
			int q = numbers[i].length;
			for(int c = 0; c < q; c++){
				int[] f = numbers[i];
				int g = f[c];
				System.out.println("  " + (i+1) + " -> " + (g+1) + ";");
			}
			
					
		}

	}

	public static void dot(int[][] numbers) {
		System.out.println("digraph G {" );
		dotEndPages(numbers);
		dotOrphanPages(numbers);
		dotPlot(numbers);
		System.out.println("}" );
	}

	public static ArrayList<Integer> getPathOfFirsts(int[][] numbers) {
		ArrayList<Integer> hello = new ArrayList<Integer>();
		hello.add(0);
		int i = 0;
		while (numbers[i].length != 0) {
			hello.add(numbers[i][0]);
			
			i = numbers[i][0];
		
		}
		return hello;
	}
}
