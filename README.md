Don't forget the coding session sandwich:

  1. sync if instructor has made edits
  2. pull from fork
  3. solve problems
  4. add/stage new files
  5. commit
  6. push
  7. verify
